<?php

namespace Drupal\scheduled_message\Plugin\QueueWorker;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\TypedData\Type\DateTimeInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\message\Entity\Message;
use Drupal\scheduled_message\QueueManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityQueue.
 *
 * (Re)schedules pending messages for each entity in the queue.
 *
 * @QueueWorker (
 *   id = "scheduled_message_entity",
 *   title = @Translation("Set up/update scheduled messages for an entity."),
 *   cron = {"time" = 20}
 * )
 */
class EntityQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\scheduled_message\QueueManager
   */
  protected $queueManager;

  /**
   * Constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManager $entityTypeManager, QueueManager $queueManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->queueManager = $queueManager;
  }

  /**
   * Class factory.
   *
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('scheduled_message.queue_manager')
    );
  }

  /**
   * Re-calculate all pending scheduled messages for an entity.
   *
   * @inheritDoc
   */
  public function processItem($data) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = isset($data->entity) ?
      $data->entity :
      $this->entityTypeManager
        ->getStorage($data->trigger_entity_type)->load($data->trigger_id);

    /** @var \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $schedule */
    $schedule = $this->entityTypeManager
      ->getStorage('scheduled_message_type')->load($data->schedule_id);

    $storage = $this->entityTypeManager->getStorage('scheduled_message');

    $messages = $storage->loadByProperties([
      'bundle' => $schedule->id(),
      'trigger' => $entity->id(),
    ]);

    $recipients = $this->queueManager->getRecipients($entity, $schedule);

    $message_list = [];
    foreach ($messages as $message) {
      $message_list[$message->recipient->target_id] = $message;
    }

    foreach ($recipients as $recipient) {
      if ($message = $message_list[$recipient->id()]) {
        if (!$message->sent->isEmpty()) {
          $this->queueManager->updateMessage($message, $entity, $recipient, $schedule);
          unset($message_list[$recipient->id()]);
        }
      }
      else {
        if ($this->queueManager->shouldSchedule($entity, $recipient, $schedule)) {
          $this->queueManager->createMessage($entity, $recipient, $schedule);
        }
      }
    }
    // If there are any messages left over, delete them.
    foreach ($message_list as $message) {
      if (!$message->sent->isEmpty()){
        $message->delete();
      }
    }
  }
}
