<?php

namespace Drupal\scheduled_message\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\scheduled_message\Entity\ScheduledMessage;
use Drupal\scheduled_message\Event\ScheduledMessageEvent;
use Drupal\scheduled_message\Event\ScheduledMessageEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Base class for queue workers to send messages at appropriate time.
 */
abstract class ScheduledMessageWorkerBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * EntityStorage for message entity.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $messageStorage;

  /**
   * MessageNotifier Service.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotifier;

  /**
   * Event Dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $eventDispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Class factory.
   *
   * @inheritdoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher')
    );
  }

  /**
   * Process a single scheduled message.
   *
   * @inheritdoc
   */
  public function processItem($data) {
    $message = ScheduledMessage::load($data->id);
    $schedule = $message->get('bundle')->entity;

    $event = new ScheduledMessageEvent($message, $schedule);
    $this->eventDispatcher->dispatch($event, ScheduledMessageEvents::SEND_MESSAGE);
  }

}
