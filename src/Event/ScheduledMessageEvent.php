<?php

namespace Drupal\scheduled_message\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\scheduled_message\Entity\ScheduledMessageInterface;
use Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface;

/**
 * Scheduled Message event.
 *
 * @see \Drupal\scheduled_message\Event\ScheduledMessageEvents
 */
class ScheduledMessageEvent extends Event {

  /**
   * The Scheduled Message.
   *
   * @var \Drupal\scheduled_message\Entity\ScheduledMessageInterface
   */
  protected $message;

  /**
   * The message schedule.
   *
   * @var \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface
   */
  protected $schedule;

  /**
   * ScheduledMessageEvent constructor.
   *
   * @param \Drupal\scheduled_message\Entity\ScheduledMessageInterface $message
   *   The message.
   * @param \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $schedule
   *   The schedule.
   */
  public function __construct(ScheduledMessageInterface $message, ScheduledMessageTypeInterface $schedule) {
    $this->message = $message;
    $this->schedule = $schedule;
  }

  /**
   * Get the message.
   *
   * @return \Drupal\scheduled_message\Entity\ScheduledMessageInterface
   *   The message.
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Get the schedule.
   *
   * @return \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface
   */
  public function getSchedule() {
    return $this->schedule;
  }

  /**
   * Get the configured message module.
   *
   * @return string
   */
  public function getMessageModule() {
    return $this->schedule->getMessageModule();
  }
}
