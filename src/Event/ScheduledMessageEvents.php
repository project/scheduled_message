<?php

namespace Drupal\scheduled_message\Event;

/**
 * Defines events for Scheduled Messages.
 *
 * @see \Drupal\scheduled_message\Event\RegistrationAccessEvent
 */
final class ScheduledMessageEvents {

  /**
   * Name of the event when it's time to send a message.
   *
   * This event allows submodules to compose and send the actual message.
   *
   * @Event
   *
   * @var string
   */
  const SEND_MESSAGE = 'scheduled_message.send_message';

}
