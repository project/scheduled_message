<?php

namespace Drupal\scheduled_message\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Scheduled Message type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "scheduled_message_type",
 *   label = @Translation("Scheduled Message"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\scheduled_message\Form\ScheduledMessageTypeForm",
 *       "edit" = "Drupal\scheduled_message\Form\ScheduledMessageTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\scheduled_message\ScheduledMessageTypeListBuilder",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer scheduled message types",
 *   bundle_of = "scheduled_message",
 *   config_prefix = "scheduled_message_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/scheduled_message_types/add",
 *     "edit-form" = "/admin/structure/scheduled_message_types/manage/{scheduled_message_type}",
 *     "delete-form" = "/admin/structure/scheduled_message_types/manage/{scheduled_message_type}/delete",
 *     "collection" = "/admin/structure/scheduled_message_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "trigger_entity_type",
 *     "trigger_entity_bundle",
 *     "trigger_field",
 *     "trigger_offset",
 *     "recipient_entity_type",
 *     "recipient_entity_bundle",
 *     "recipient_field",
 *     "condition_trigger_status",
 *     "condition_recipient_state_field",
 *     "condition_recipient_state",
 *     "condition_link_entity",
 *     "condition_link_field",
 *     "message_module",
 *     "message_template",
 *     "field_map"
 *   }
 * )
 */
class ScheduledMessageType extends ConfigEntityBundleBase implements ScheduledMessageTypeInterface {

  /**
   * The machine name of this scheduled message type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the scheduled message type.
   *
   * @var string
   */
  protected $label;

  /**
   * @var string
   */
  protected $trigger_entity_type;

  /**
   * @var string
   */
  protected $trigger_entity_bundle;

  /**
   * @var string
   */
  protected $trigger_field;

  /**
   * @var string
   */
  protected $trigger_offset;

  /**
   * @var string
   */
  protected $recipient_entity_type;

  /**
   * @var string
   */
  protected $recipient_entity_bundle;

  /**
   * @var string
   */
  protected $recipient_field;

  /**
   * @var string
   */
  protected $condition_trigger_status;

  /**
   * @var string
   */
  protected $condition_recipient_state;

  /**
   * @var string
   */
  protected $condition_recipient_state_field;

  /**
   * @var string
   */
  protected $condition_link_entity;

  /**
   * @var string
   */
  protected $condition_link_field;

  /**
   * @var string
   */
  protected $message_module;

  /**
   * @var string
   */
  protected $message_template;
  /**
   * @var string
   */
  protected $field_map;

  /**
   * @inheritDoc
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @inheritDoc
   */
  public function setId($id) {
    $this->id = $id;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * @inheritDoc
   */
  public function setLabel($label) {
    $this->label = $label;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getTriggerEntityType() {
    return $this->trigger_entity_type;
  }

  /**
   * @inheritDoc
   */
  public function setTriggerEntityType($entity_type) {
    $this->trigger_entity_type = $entity_type;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getTriggerEntityBundle() {
    return $this->trigger_entity_bundle;
  }

  /**
   * @inheritDoc
   */
  public function setTriggerEntityBundle($bundle) {
    $this->trigger_entity_bundle = $bundle;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getTriggerField() {
    return $this->trigger_field;
  }

  /**
   * @inheritDoc
   */
  public function setTriggerField($field_name) {
    $this->trigger_field = $field_name;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getTriggerOffset() {
    return $this->trigger_offset;
  }

  /**
   * @inheritDoc
   */
  public function setTriggerOffset($offset) {
    $this->trigger_offset = $offset;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getRecipientEntityType() {
    return $this->recipient_entity_type;
  }

  /**
   * @inheritDoc
   */
  public function setRecipientEntityType($entity_type) {
    $this->recipient_entity_type = $entity_type;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getRecipientEntityBundle() {
    return $this->recipient_entity_bundle;
  }

  /**
   * @inheritDoc
   */
  public function setRecipientEntityBundle($bundle) {
    $this->recipient_entity_bundle = $bundle;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getRecipientField() {
    return $this->recipient_field;
  }

  /**
   * @inheritDoc
   */
  public function setRecipientField($field_name) {
    $this->recipient_field = $field_name;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getConditionTriggerStatus() {
    return $this->condition_trigger_status;
  }

  /**
   * @inheritDoc
   */
  public function setConditionTriggerStatus($status) {
    $this->condition_trigger_status = $status;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getRecipientState() {
    return $this->condition_recipient_state;
  }

  /**
   * @inheritDoc
   */
  public function setRecipientState($state) {
    $this->condition_recipient_state = $state;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getRecipientStateField() {
    return $this->condition_recipient_state_field;
  }

  /**
   * @inheritDoc
   */
  public function setRecipientStateField($field_name) {
    $this->condition_recipient_state_field = $field_name;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getLinkEntity() {
    return $this->condition_link_entity;
  }

  /**
   * @inheritDoc
   */
  public function setLinkEntity($link_entity) {
    $this->condition_link_entity = $link_entity;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getLinkField() {
    return $this->condition_link_field;
  }

  /**
   * @inheritDoc
   */
  public function setLinkField($field_name) {
    $this->condition_link_field = $field_name;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getMessageModule() {
    return $this->message_module;
  }

  /**
   * @inheritDoc
   */
  public function setMessageModule($message_module) {
    $this->message_module = $message_module;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getFieldMap() {
    $lines = explode("\r\n", $this->field_map);
    $map = [];
    foreach ($lines as $line) {
      $item = explode(':', $line);
      $map[trim($item[0])] = trim($item[1]);
    }
    return $map;
  }

  /**
   * @inheritDoc
   */
  public function setFieldMap($field_map) {
    $this->field_map = $field_map;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getMessageTemplate() {
    return $this->message_template;
  }

  /**
   * @inheritDoc
   */
  public function setMessageTemplate($message_template) {
    $this->message_template = $message_template;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getRawFieldMap() {
    return $this->field_map;
  }
}
