<?php

namespace Drupal\scheduled_message\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\user\UserInterface;

/**
 * Defines the scheduled message entity class.
 *
 * @ContentEntityType(
 *   id = "scheduled_message",
 *   label = @Translation("Scheduled Message"),
 *   label_collection = @Translation("Scheduled Messages"),
 *   bundle_label = @Translation("Scheduled Message type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\scheduled_message\ScheduledMessageListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\scheduled_message\ScheduledMessageAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\scheduled_message\Form\ScheduledMessageForm",
 *       "add" = "Drupal\scheduled_message\Form\ScheduledMessageForm",
 *       "edit" = "Drupal\scheduled_message\Form\ScheduledMessageForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "scheduled_message",
 *   data_table = "scheduled_message_field_data",
 *   admin_permission = "administer scheduled message types",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/scheduled-message/add/{scheduled_message_type}",
 *     "add-page" = "/admin/content/scheduled-message/add",
 *     "canonical" = "/scheduled_message/{scheduled_message}",
 *     "edit-form" = "/admin/content/scheduled-message/{scheduled_message}/edit",
 *     "delete-form" = "/admin/content/scheduled-message/{scheduled_message}/delete",
 *     "collection" = "/admin/content/scheduled-message"
 *   },
 *   bundle_entity_type = "scheduled_message_type",
 *   field_ui_base_route = "entity.scheduled_message_type.edit_form"
 * )
 */
class ScheduledMessage extends ContentEntityBase implements ScheduledMessageInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new scheduled message entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the scheduled message entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the scheduled message.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the scheduled message author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the scheduled message was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the scheduled message was last edited.'));

    $fields['trigger'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Trigger'))
      ->setDescription(t('The triggering entity'))
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ]);

    $fields['recipient'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Recipient'))
      ->setDescription(t('The recipient entity'))
      ->setSetting('handler', 'default')
      ->setSetting('target_type', 'node') // this will be overridden in the bundle
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ]);

    $fields['send_at'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Send at'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 0,
        'type' => 'datetime_plain',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 20,
      ])
      ->setDescription(t('The time to send the message.'));

    $fields['sent'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date/time sent'))
      ->setDescription(t('Indicates the time this message was sent.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 0,
        'type' => 'datetime_plain',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 20,
      ])
      ->setRequired(FALSE);

    return $fields;
  }

  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions); // TODO: Change the autogenerated stub

    /** @var \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $sm_type */
    $sm_type = ScheduledMessageType::load($bundle);

    $trigger = clone $base_field_definitions['trigger'];
    $trigger->setSetting('target_type', $sm_type->getTriggerEntityType());
    $target_bundle = $sm_type->getTriggerEntityBundle();
    if (!empty($target_bundle)) {
      $trigger->setSetting('handler_settings', [
        'target_bundles' => [
          $target_bundle => $target_bundle,
        ],
      ]);
    }
    $fields['trigger'] = $trigger;

    $recipient = clone $base_field_definitions['recipient'];
    $recipient->setSetting('target_type', $sm_type->getRecipientEntityType());
    $target_bundle = $sm_type->getRecipientEntityBundle();
    if (!empty($target_bundle)) {
      $recipient->setSetting('handler_settings', [
        'target_bundles' => [
          $target_bundle => $target_bundle,
        ],
      ]);
    }
    $fields['recipient'] = $recipient;

    return $fields;
  }

  /**
   * @inheritDoc
   */
  public function getSendAtTime() {
    return $this->send_at->value;
  }

  /**
   * @inheritDoc
   */
  public function setSendAtTime($datetime) {
    $this->send_at->value = $datetime;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getSent() {
    return $this->sent->value;
  }

  /**
   * @inheritDoc
   */
  public function setSent($datetime = NULL) {
    if (empty($datetime)) {
      $datetime = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    }
    $this->sent->value = $datetime;
    return $this;
  }
}
