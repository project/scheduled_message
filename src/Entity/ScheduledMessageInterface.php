<?php

namespace Drupal\scheduled_message\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a scheduled message entity type.
 */
interface ScheduledMessageInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the scheduled message title.
   *
   * @return string
   *   Title of the scheduled message.
   */
  public function getTitle();

  /**
   * Sets the scheduled message title.
   *
   * @param string $title
   *   The scheduled message title.
   *
   * @return \Drupal\scheduled_message\Entity\ScheduledMessageInterface
   *   The called scheduled message entity.
   */
  public function setTitle($title);

  /**
   * Gets the scheduled message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the scheduled message.
   */
  public function getCreatedTime();

  /**
   * Sets the scheduled message creation timestamp.
   *
   * @param int $timestamp
   *   The scheduled message creation timestamp.
   *
   * @return \Drupal\scheduled_message\Entity\ScheduledMessageInterface
   *   The called scheduled message entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the send at time.
   *
   * @return string Date string
   *   The send at date string.
   */
  public function getSendAtTime();

  /**
   * Set the send at time.
   *
   * @param string $datetime
   *   The date to send at.
   *
   * @return \Drupal\scheduled_message\Entity\ScheduledMessageInterface
   *   The called scheduled message entity.
   */
  public function setSendAtTime($datetime);

  /**
   * Get the time the message was sent.
   *
   * @return null|string
   *   Sent time, if sent.
   */
  public function getSent();

  /**
   * Set the sent time to now.
   *
   * @param string|null $datetime
   *   Date/Timestamp message was sent, or now if not specified.
   *
   * @return \Drupal\scheduled_message\Entity\ScheduledMessageInterface
   *   The called scheduled message entity.
   */
  public function setSent($datetime = null);

}
