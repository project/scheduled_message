<?php

namespace Drupal\scheduled_message\Entity;


use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface ScheduledMessageTypeInterface extends ConfigEntityInterface {

  /**
   * @return string
   */
  public function getId();

  /**
   * @param string $id
   *
   * @return $this
   */
  public function setId($id);

  /**
   * @return string
   */
  public function getLabel();

  /**
   * @param string $label
   *
   * @return $this
   */
  public function setLabel($label);

  /**
   * @return string
   */
  public function getTriggerEntityType();

  /**
   * @param string $entity_type
   *
   * @return ScheduledMessageType
   */
  public function setTriggerEntityType($entity_type);

  /**
   * @return string
   */
  public function getTriggerEntityBundle();

  /**
   * @param string $bundle
   *
   * @return ScheduledMessageType
   */
  public function setTriggerEntityBundle($bundle);

  /**
   * @return string
   */
  public function getTriggerField();

  /**
   * @param string $field_name
   *
   * @return ScheduledMessageType
   */
  public function setTriggerField($field_name);

  /**
   * @return string
   */
  public function getTriggerOffset();

  /**
   * @param string $offset
   *
   * @return ScheduledMessageType
   */
  public function setTriggerOffset($offset);

  /**
   * @return string
   */
  public function getRecipientEntityType();

  /**
   * @param string $entity_type
   *
   * @return ScheduledMessageType
   */
  public function setRecipientEntityType($entity_type);

  /**
   * @return string
   */
  public function getRecipientEntityBundle();

  /**
   * @param string $bundle
   *
   * @return ScheduledMessageType
   */
  public function setRecipientEntityBundle($bundle);

  /**
   * @return string
   */
  public function getRecipientField();

  /**
   * @param string $field_name
   *
   * @return ScheduledMessageType
   */
  public function setRecipientField($field_name);

  /**
   * @return int|null
   */
  public function getConditionTriggerStatus();

  /**
   * Status of trigger entity to match.
   *
   * @param int|null $status
   *
   * @return ScheduledMessageType
   */
  public function setConditionTriggerStatus($status);

  /**
   * Value of a workflow or state machine state.
   *
   * @return string
   */
  public function getRecipientState();

  /**
   * @param string $state
   *
   * @return ScheduledMessageType
   */
  public function setRecipientState($state);

  /**
   * Field containing workflow or state machine state.
   *
   * @return string
   */
  public function getRecipientStateField();

  /**
   * @param string $field_name
   *
   * @return ScheduledMessageType
   */
  public function setRecipientStateField($field_name);

  /**
   * The entity with the entityreference field pointing to the other entity -
   *   "trigger" or "recipient"
   *
   * @return string
   */
  public function getLinkEntity();

  /**
   * @param string $link_entity
   *
   * @return ScheduledMessageType
   */
  public function setLinkEntity($link_entity);

  /**
   * The field name of the entityreference field pointing to the other entity.
   *
   * @return string
   */
  public function getLinkField();

  /**
   * @param string $field_name
   *
   * @return ScheduledMessageType
   */
  public function setLinkField($field_name);

  /**
   * The module managing the email template and sending.
   *
   * TODO: replace with plugin
   *
   * @return string
   */
  public function getMessageModule();

  /**
   * @param string $message_module
   *
   * @return ScheduledMessageType
   */
  public function setMessageModule($message_module);

  /**
   * The module managing the email template and sending.
   *
   * TODO: replace with plugin
   *
   * @return string
   */
  public function getMessageTemplate();

  /**
   * @param string $message_template
   *
   * @return ScheduledMessageType
   */
  public function setMessageTemplate($message_template);
  /**
   * Map of fields to copy data, in the form:
   * message_module_field_name: trigger_field_name
   * message_module_field_name2: @recipient_field_name
   *
   * use "W" for fields on the recipient.
   *
   * "id" will populate an entity_reference.
   *
   * @return array
   */
  public function getFieldMap();

  /**
   * String value of field map for editing form.
   *
   * @return string
   */
  public function getRawFieldMap();

  /**
   * @param string $field_map
   *
   * @return ScheduledMessageType
   */
  public function setFieldMap($field_map);


}
