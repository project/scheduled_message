<?php

namespace Drupal\scheduled_message;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the scheduled message entity type.
 */
class ScheduledMessageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view scheduled message');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit scheduled message', 'administer scheduled message'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete scheduled message', 'administer scheduled message'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create scheduled message', 'administer scheduled message'], 'OR');
  }

}
