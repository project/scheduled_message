<?php

namespace Drupal\scheduled_message;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\TypedData\Type\DateTimeInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\scheduled_message\Entity\ScheduledMessageInterface;
use Drupal\scheduled_message\Entity\ScheduledMessageType;
use Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface;

/**
 * Class QueueManager.
 *
 * @package Drupal\scheduled_message
 */
class QueueManager {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The QueueFactory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueueFactory $queueFactory, ModuleHandlerInterface $moduleHandler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queueFactory = $queueFactory;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Find messages that are scheduled to send now, and add to send queue.
   */
  public function queueMessages() {
    $storage = $this->entityTypeManager->getStorage('scheduled_message');
    $query = $storage->getQuery();

    $today = new DrupalDateTime();
    $today->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $today_date = $today->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);

    $queue = $this->queueFactory->get('cron_scheduled_message');
    $message_ids = $query->notExists('sent')
      ->condition('send_at', $today_date, '<=')
      ->execute();
    foreach ($message_ids as $message_id) {
      $item = new \stdClass();
      $item->id = $message_id;
      $queue->createItem($item);
    }
  }

  /**
   * Queue an individual entity for scheduling.
   *
   * Checks for this message in the queue, and adds if it does not exist, or
   * updates if it does exist. If no longer relevant, delete.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $trigger
   *   The entity to schedule/reschedule.
   * @param \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $schedule
   *   The Scheduled Message config
   */
  public function queueTriggerEntity(ContentEntityInterface $trigger, ScheduledMessageTypeInterface $schedule) {
    $queue = $this->queueFactory->get('scheduled_message_entity');
    $item = new \stdClass();
    $item->trigger_id = $trigger->id();
    $item->trigger_entity_type = $trigger->getEntityTypeId();
    $item->schedule_id = $schedule->id();
    $queue->createItem($item);
  }

  /**
   * Queue individual messages to send.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $recipient
   *   Recipient entity.
   * @param \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $schedule
   *   The schedule.
   */
  public function queueRecipientEntity(ContentEntityInterface $recipient, ScheduledMessageTypeInterface $schedule) {
    $storage = $this->entityTypeManager->getStorage('scheduled_message');
    $scheduled_messages = $storage->loadByProperties([
      'bundle' => $schedule->bundle(),
      'recipient' => $recipient->id(),
    ]);
    $trigger = $this->getTrigger($recipient, $schedule);
    if ($this->shouldSchedule($trigger, $recipient, $schedule)) {
      // create or update entity
      $date = $this->calculateDate($trigger, $recipient, $schedule);
      if (count($scheduled_messages)) {
        $scheduled_message = reset($scheduled_messages);
        if ($scheduled_messages->sent->isEmpty()) {
          $this->updateMessage($scheduled_message, $trigger, $recipient, $schedule);
        }
      }
      else {
        $this->createMessage($trigger, $recipient, $schedule);
      }
    }
    elseif (count($scheduled_messages)) {
      // delete
      foreach ($scheduled_messages as $scheduled_message) {
        $scheduled_message->delete();
      }
    }
  }

  /**
   * Create a Scheduled Message entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $trigger
   * @param \Drupal\Core\Entity\ContentEntityInterface $recipient
   * @param \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $schedule
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  function createMessage(ContentEntityInterface $trigger, ContentEntityInterface $recipient, ScheduledMessageTypeInterface $schedule) {
    if ($recipient->hasField('uid')) {
      $author = $recipient->uid->target_id;
    }
    elseif ($trigger->hasField('uid')) {
      $author = $trigger->uid->target_id;
    }
    else {
      $author = \Drupal::currentUser();
    }
    $date = $this->calculateDate($trigger, $recipient, $schedule);
    $storage = $this->entityTypeManager->getStorage('scheduled_message');
    $scheduled_message = $storage->create([
      'bundle' => $schedule->id(),
      'title' => $schedule->label() . ' - ' . $recipient->label(),
      'uid' => $author,
      'trigger' => $trigger->id(),
      'recipient' => $recipient->id(),
      'send_at' => $date,
    ]);
    $scheduled_message->save();
  }

  function updateMessage(ScheduledMessageInterface $message, ContentEntityInterface $trigger, ContentEntityInterface $recipient, ScheduledMessageTypeInterface $schedule) {
    $changed = FALSE;

    $date = $this->calculateDate($trigger, $recipient, $schedule);

    if ($message->send_at->value != $date) {
      $message->set('send_at', $date);
      $changed = TRUE;
    }

    if ($changed) {
      $message->save();
    }
  }

  /**
   * Get the trigger entity, given the recipient and the schedule.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $recipient
   * @param \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $schedule
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  function getTrigger(ContentEntityInterface $recipient, ScheduledMessageTypeInterface $schedule) {
    $trigger_type = $schedule->getTriggerEntityType();
    $linkField = $schedule->getLinkField();
    $linkEntity = $schedule->getLinkEntity();
    $storage = $this->entityTypeManager->getStorage($trigger_type);
    if ($linkEntity == 'recipient') {
      $target_id = $recipient->get($linkField)->target_id;
      $trigger = $storage->load($target_id);
      return $trigger;
    } else {
      $bundle = $schedule->getTriggerEntityBundle();
      $bundleKey = $this->entityTypeManager->getDefinition($trigger_type)->getKey('bundle');
      $triggers = $storage->loadByProperties([
        $bundleKey => $bundle,
        $linkField . '__target_id' => $recipient->id(),
      ]);
      return reset($triggers);
    }
  }

  /**
   * Get recipients for schedule based on trigger.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $trigger
   * @param \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $schedule
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  function getRecipients(ContentEntityInterface $trigger, ScheduledMessageTypeInterface $schedule) {
    $recipient_type = $schedule->getRecipientEntityType();
    $linkEntity = $schedule->getLinkEntity();
    $linkField = $schedule->getLinkField();
    $storage = $this->entityTypeManager->getStorage($recipient_type);
    if ($linkEntity == 'recipient') {
      $bundle = $schedule->getRecipientEntityBundle();
      $bundleKey = $this->entityTypeManager->getDefinition($recipient_type)->getKey('bundle');
      $recipients = $storage->loadByProperties([
        $bundleKey => $bundle,
        $linkField . '__target_id' => $trigger->id(),
      ]);
      return $recipients;
    }
    else {
      $id_map = $trigger->get($linkField)->getValue();
      $ids = array_map(function($item){
        return $item['target_id'];
      }, $id_map);
      return $storage->loadMultiple($ids);
    }
  }
  /**
   * Whether there should be a message for this schedule, trigger, and recipient combination.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $trigger
   * @param \Drupal\Core\Entity\ContentEntityInterface $recipient
   * @param \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $schedule
   *
   * @return bool
   */
  function shouldSchedule(ContentEntityInterface $trigger, ContentEntityInterface $recipient, ScheduledMessageTypeInterface $schedule) {
    $trigger_status = $schedule->getConditionTriggerStatus();
    $state_field = $schedule->getRecipientStateField();
    $state = $schedule->getRecipientState();

    // For time comparison:
    $today = new DrupalDateTime();
    $today->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $today_formatted = $today->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $date = $this->calculateDate($trigger, $recipient, $schedule);
    if ($date < $today_formatted) {
      return FALSE;
    }

    if ($trigger_status !== NULL && method_exists($trigger, 'isPublished')) {
      if ($trigger_status != $trigger->isPublished()) {
        return FALSE;
      }
    }
    if (!empty($state_field) && $recipient->get($state_field)->getValue() != $state) {
      return FALSE;
    }
    $data = [];
    $results = $this->moduleHandler->alter('scheduled_message_condition', $data, $trigger, $recipient, $schedule);
    foreach ($data as $value) {
      if ($value === FALSE) {
        return FALSE;
      }
    }
    return TRUE;
  }

  function calculateDate(ContentEntityInterface $trigger, ContentEntityInterface $recipient, ScheduledMessageTypeInterface $schedule) {
    $date_field = $schedule->getTriggerField();
    $offset = $schedule->getTriggerOffset();
    $dateObj = new DrupalDateTime($trigger->get($date_field)->value);
    $dateObj->modify($offset);
    $dateObj->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    return $dateObj->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
  }

  /**
   * Add entities to the queue.
   *
   * @param array $entities
   *   The entities to add.
   * @param string $config_type
   *   The entity bundle.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entityType
   *   The entity type config entity.
   */
  public function queueEntities(array $entities, $config_type, ConfigEntityInterface $entityType) {
    $queue = $this->queueFactory->get('scheduled_message_entity');
    foreach ($entities as $entity) {
      $item = new \stdClass();
      $item->entity_id = $entity->id();
      $item->entityStorageType = $config_type;
      $item->entityType = $entityType->getEntityTypeId();
      $item->entityTypeId = $entityType->id();
      $queue->createItem($item);
    }

  }

  /**
   * Generate all messages listed on Type, according to plugin settings.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity to generate scheduled messages.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $type
   *   Entity Type to find schedule.
   */
  public function generateScheduledMessages(ContentEntityInterface $entity, ConfigEntityInterface $type) {

  }

}
