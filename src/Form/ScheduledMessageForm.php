<?php

namespace Drupal\scheduled_message\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the scheduled message entity edit forms.
 */
class ScheduledMessageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New scheduled message %label has been created.', $message_arguments));
      $this->logger('scheduled_message')->notice('Created new scheduled message %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The scheduled message %label has been updated.', $message_arguments));
      $this->logger('scheduled_message')->notice('Updated new scheduled message %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.scheduled_message.canonical', ['scheduled_message' => $entity->id()]);
  }

}
