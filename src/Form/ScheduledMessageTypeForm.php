<?php

namespace Drupal\scheduled_message\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for scheduled message type forms.
 */
class ScheduledMessageTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $entity_type */
    $entity_type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add scheduled message type');
    }
    else {
      $form['#title'] = $this->t(
        'Edit %label scheduled message type',
        ['%label' => $entity_type->label()]
      );
    }

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $entity_type->label(),
      '#description' => $this->t('The human-readable name of this scheduled message type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\scheduled_message\Entity\ScheduledMessageType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this scheduled message type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    $form['trigger'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Trigger'),
    ];

    $form['trigger']['trigger_entity_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity Type'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getTriggerEntityType(),
    ];

    $form['trigger']['trigger_entity_bundle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity Bundle'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getTriggerEntityBundle(),
    ];

    $form['trigger']['trigger_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trigger Date Field'),
      '#description' => $this->t('A date field or timestamp to use to determine when to send the message.'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getTriggerField(),
    ];

    $form['trigger']['trigger_offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Offset'),
      '#description' => $this->t('Offset from the date field to send this message. Can be any strtotime offset.'),
      '#placeholder' => '-1 day',
      '#maxlength' => 255,
      '#default_value' => $entity_type->getTriggerOffset(),
    ];

    $form['recipient'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Recipient'),
    ];

    $form['recipient']['recipient_entity_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity Type'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getRecipientEntityType(),
    ];

    $form['recipient']['recipient_entity_bundle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity Bundle'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getRecipientEntityBundle(),
    ];

    $form['recipient']['recipient_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email field name'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getRecipientField(),
    ];

    $form['condition'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Condition'),
    ];

    $form['condition']['condition_trigger_status'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trigger Status'),
      '#description' => $this->t('If not blank, only schedule message if the status of the trigger entity matches this value.'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getConditionTriggerStatus(),
    ];

    $form['condition']['condition_recipient_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recipient state'),
      '#description' => $this->t('If not blank, only schedule message if the value of the state field on the recipient matches this value.'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getRecipientState(),
    ];

    $form['condition']['condition_recipient_state_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recipient state field'),
      '#description' => $this->t('Field name on the recipient to check for the previous value. Can be boolean, string, workflow, state_machine, etc.'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getRecipientStateField(),
    ];

    $form['condition']['condition_link_entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link Entity'),
      '#description' => $this->t('Recipient and Trigger entities must be linked. Which one has the field to use? "recipient" or "trigger".'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getLinkEntity(),
    ];

    $form['condition']['condition_link_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link Field'),
      '#description' => $this->t('Field name linking the recipient and trigger.'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getLinkField(),
    ];

    $form['message_module'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message module'),
      '#description' => $this->t('Which message module owns the template to send? Currently only easy_email or message.'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getMessageModule(),
    ];

    $form['message_template'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message Template ID'),
      '#description' => $this->t('Which message template to send?'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->getMessageTemplate(),
    ];

    $form['field_map'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Field map'),
      '#rows' => 5,
      '#description' => $this->t('Map fields from trigger entity or @recipient entity to message entity.'),
      '#default_value' => $entity_type->getRawFieldMap(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save scheduled message type');
    $actions['delete']['#value'] = $this->t('Delete scheduled message type');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\scheduled_message\Entity\ScheduledMessageTypeInterface $entity_type */
    $entity_type = $this->entity;

    $entity_type->set('id', trim($entity_type->id()));
    $entity_type->set('label', trim($entity_type->label()));

    $entity_type->setTriggerEntityType(trim($form_state->getValue('trigger_entity_type')))
      ->setTriggerEntityBundle(trim($form_state->getValue('trigger_entity_bundle')))
      ->setTriggerField(trim($form_state->getValue('trigger_field')))
      ->setTriggerOffset(trim($form_state->getValue('trigger_offset')))
      ->setRecipientEntityType(trim($form_state->getValue('recipient_entity_type')))
      ->setRecipientEntityBundle(trim($form_state->getValue('recipient_entity_bundle')))
      ->setRecipientField(trim($form_state->getValue('recipient_field')))
      ->setConditionTriggerStatus(trim($form_state->getValue('condition_trigger_status')))
      ->setRecipientState(trim($form_state->getValue('condition_recipient_state')))
      ->setRecipientStateField(trim($form_state->getValue('condition_recipient_state_field')))
      ->setLinkEntity(trim($form_state->getValue('condition_link_entity')))
      ->setLinkField(trim($form_state->getValue('condition_link_field')))
      ->setMessageModule(trim($form_state->getValue('message_module')))
      ->setMessageTemplate(trim($form_state->getValue('message_template')))
      ->setFieldMap(trim($form_state->getValue('field_map')));

    $status = $entity_type->save();

    $t_args = ['%name' => $entity_type->label()];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The scheduled message type %name has been updated.', $t_args);
    }
    elseif ($status == SAVED_NEW) {
      $message = $this->t('The scheduled message type %name has been added.', $t_args);
    }
    $this->messenger()->addStatus($message);

    $form_state->setRedirectUrl($entity_type->toUrl('collection'));
  }

}
