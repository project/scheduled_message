<?php

namespace Drupal\scheduled_message;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of scheduled message type entities.
 *
 * @see \Drupal\scheduled_message\Entity\ScheduledMessageType
 */
class ScheduledMessageTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Label');
    $header['trigger_entity'] = $this->t('Trigger Entity');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];
    $row['trigger_entity'] = [
      'data' => $entity->getTriggerEntityType() . '.' . $entity->getTriggerEntityBundle(),
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No scheduled message types available. <a href=":link">Add scheduled message type</a>.',
      [':link' => Url::fromRoute('entity.scheduled_message_type.add_form')->toString()]
    );

    return $build;
  }

}
