<?php

namespace Drupal\scheduled_message_message\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\easy_email\Service\EmailHandlerInterface;
use Drupal\message_notify\MessageNotifier;
use Drupal\scheduled_message\Event\ScheduledMessageEvent;
use Drupal\scheduled_message\Event\ScheduledMessageEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Scheduled Message Message event subscriber.
 */
class ScheduledMessageMessageSubscriber implements EventSubscriberInterface {

  /**
   * EntityType Manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * MessageNotifier Service.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotifier;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\easy_email\Service\EmailHandlerInterface $easy_email_handler
   */
  public function __construct(EmailHandlerInterface $easy_email_handler, EntityTypeManager $entityTypeManager, MessageNotifier $messageNotifier) {
    $this->entityTypeManager = $entityTypeManager;
    $this->messageNotifier = $messageNotifier;
  }

  /**
   * Scheduled Message event handler.
   *
   * @param \Drupal\scheduled_message\Event\ScheduledMessageEvent $event
   *   Response event.
   */
  public function onSendMessage(ScheduledMessageEvent $event) {
    if ($event->getMessageModule() == 'message') {
      $message = $event->getMessage();
      $schedule = $event->getSchedule();
      if (empty($message->getSent())) {
        $trigger = $message->trigger->entity;
        $recipient = $message->recipient->entity;
        $data = [
          'type' => $schedule->getMessageTemplate(),
        ];
        $storage = $this->entityTypeManager->getStorage('message');
        $email = $storage->create($data);

        $map = $schedule->getFieldMap();
        foreach ($map as $key => $value) {
          if (strpos($value, '@') === 0) {
            $field = substr($value, 1);
            if ($field == 'id') {
              $field_value = $recipient->id();
            }
            else {
              $field_value = $recipient->get($field)->getValue();
            }
          }
          else {
            if ($value == 'id') {
              $field_value = $trigger->id();
            }
            else {
              $field_value = $trigger->get($value)->getValue();
            }
          }
          if (!empty($field_value)) {
            $email->set($key, $field_value);
          }
        }
        $this->messageNotifier->send($email);
        $email->save();

        $message->setSent();
        $message->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ScheduledMessageEvents::SEND_MESSAGE => ['onSendMessage'],
    ];
  }

}
