<?php

namespace Drupal\scheduled_message_easy_email\EventSubscriber;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\easy_email\Service\EmailHandlerInterface;
use Drupal\scheduled_message\Event\ScheduledMessageEvent;
use Drupal\scheduled_message\Event\ScheduledMessageEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * scheduled_message_easy_email event subscriber.
 */
class ScheduledMessageEasyEmailSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var EmailHandlerInterface
   */
  protected $easyEmailHandler;

  /**
   * Email Validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\easy_email\Service\EmailHandlerInterface $easy_email_handler
   *   Easy Email handler.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   Email Validator.
   */
  public function __construct(EmailHandlerInterface $easy_email_handler, EmailValidatorInterface $email_validator) {
    $this->easyEmailHandler = $easy_email_handler;
    $this->emailValidator = $email_validator;
  }

  /**
   * Scheduled Message event handler.
   *
   * @param \Drupal\scheduled_message\Event\ScheduledMessageEvent $event
   *   Response event.
   */
  public function onSendMessage(ScheduledMessageEvent $event) {
    if ($event->getMessageModule() == 'easy_email') {
      $message = $event->getMessage();
      $schedule = $event->getSchedule();
      if (empty($message->getSent())) {
        $trigger = $message->trigger->entity;
        $recipient = $message->recipient->entity;
        $email_field = $schedule->getRecipientField();
        if (!$this->emailValidator->isValid($recipient->get($email_field)->value)) {
          return;
        }
        $data = [
          'type' => $schedule->getMessageTemplate(),
        ];
        $email = $this->easyEmailHandler->createEmail($data);
        $map = $schedule->getFieldMap();
        foreach ($map as $key => $value) {
          if (strpos($value, '@') === 0) {
            $field = substr($value, 1);
            if ($field == 'id') {
              $field_value = $recipient->id();
            }
            else {
              $field_value = $recipient->get($field)->getValue();
            }
          }
          else {
            if ($value == 'id') {
              $field_value = $trigger->id();
            }
            else {
              $field_value = $trigger->get($value)->getValue();
            }
          }
          if (!empty($field_value)) {
            $email->set($key, $field_value);
          }
        }
        if (!$this->easyEmailHandler->duplicateExists($email)) {
          $this->easyEmailHandler->sendEmail($email);
        }
        $message->setSent();
        $message->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ScheduledMessageEvents::SEND_MESSAGE => ['onSendMessage'],
    ];
  }

}
